package coroutines.experiments

import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

fun main() = logTime {
    runBlocking {
        val result = (1..1000).map {
            async {
                delay(timeMillis = 1000)
                it
            }
        }.awaitAll().sum()
        println("result = $result")
    }
}