package coroutines.experiments

import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

fun main() {
    logTime {
        val result = runBlocking {
            val five = async {
                delay(10000)
                return@async 5
            }
            val six = async {
                delay(8000)
                return@async 6
            }
            return@runBlocking five.await() + six.await()
        }
        println("result = $result")
    }
}