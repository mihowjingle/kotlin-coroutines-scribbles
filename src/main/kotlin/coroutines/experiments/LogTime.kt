package coroutines.experiments

import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

inline fun <T> logTime(startLabel: String = "start: ", endLabel: String = "end:   ", pattern: String = "HH:mm:ss.SSS", block: () -> T): T {
    val formatter = DateTimeFormatter.ofPattern(pattern)
    val start = LocalDateTime.now()
    println(startLabel + start.format(formatter))
    val result = block()
    val end = LocalDateTime.now()
    val duration = Duration.between(start, end)
    println("$endLabel${end.format(formatter)}, duration: ${duration.toMillis().toDouble() / 1000} seconds")
    return result
}