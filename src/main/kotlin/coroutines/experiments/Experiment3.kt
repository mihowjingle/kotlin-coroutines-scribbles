package coroutines.experiments

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    logTime {
        runBlocking {
            val channel = Channel<Int>()
            launch {
                delay(5000)
                channel.send(6)
            }
            launch {
                delay(3000)
                val number = channel.receive()
                println("five seconds passed (not three) and six = $number")
            }
        }
    }
}