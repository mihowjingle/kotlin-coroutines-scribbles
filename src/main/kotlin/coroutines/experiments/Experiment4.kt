package coroutines.experiments

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    logTime {
        runBlocking {
            val channel = Channel<Int>(Channel.CONFLATED)
            launch {
                delay(1000)
                channel.send(6)
                delay(1000)
                channel.send(9)
                delay(1000)
                channel.send(0)
            }
            launch {
                delay(2600)
                var number = channel.receive()
                println("almost three seconds passed and nine = $number")
                number = channel.receive()
                println("three seconds passed and zero = $number")
            }
        }
    }
}